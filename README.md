# BTS App

## Purpose
For React and JS learning - started Jan 2023 

## App overview (MVP)
This will be a BTS fan-made wiki with information of the group, each member, a database of songs, music videos and Bangtan bombs which the end-user can sort, search and view. 

## Overall features or JS/React topics
- HTML and CSS
- Mobile first / responsive screens
- Navbar 
- Carousel slider 
- Routes
- Forms 
- Search/look up functionality 
- Sorting data
- Viewing data
- State management / redux
- Arrays (lists)
- Map() 
- Async()  

## Libraries used 


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.